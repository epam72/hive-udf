package epam.udf;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.UDFType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@SuppressWarnings("deprecation")
@Description(
  name = "RandomDateWithinInterval",
  value = "_FUNC_ returns random date within provided date range"
)
@UDFType(deterministic = false)
public class RandomDate extends UDF {
  /**
   * Get random date for provided date range
   *
   * @param dateFrom start date
   * @param dateTo   end date
   * @return arbitrary date
   * @throws HiveException
   */
  public String evaluate(String dateFrom, String dateTo) throws HiveException {
    try {
      LocalDate df = LocalDate.parse(dateFrom);
      LocalDate dt = LocalDate.parse(dateTo);

      if (df.isAfter(dt)) throw new Exception("dateFrom must be  equal or after dateTo");

      long diff = dt.toEpochDay() - df.toEpochDay();
      return df
        .plusDays(new Random().nextInt((int) diff))
        .format(DateTimeFormatter.ISO_LOCAL_DATE);
    } catch (Throwable th) {
      throw new HiveException(th);
    }
  }
}
