package epam.udf;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.metadata.HiveException;

@SuppressWarnings("deprecation")
@Description(
  name = "geohash",
  value = "_FUNC_ returns geohash for provided latitude and longitude with specified accuracy"
)
public final class GeoHash extends UDF {

  /**
   *
   * @param lat Latitude
   * @param lon Longitude
   * @param hashSize required hash accuracy
   * @return hash
   * @throws HiveException
   */
  public String evaluate(Double lat, Double lon, Integer hashSize) throws HiveException {
    if (lat == null || lat.isNaN() || lat.isInfinite()
      || lon == null || lon.isNaN() || lon.isInfinite()) {
      return null;
    } else if (hashSize == null || hashSize < 1 || hashSize > 8) {
      throw new HiveException("InvalidHashSize");
    } else
      return ch.hsr.geohash.GeoHash.withCharacterPrecision(lat, lon, hashSize).toBase32();
  }

  /**
   * Default wrapper for getting 4 char length hashes
   *
   * @param lat Latitude
   * @param lon Longitude
   * @return hash
   * @throws HiveException
   */
  public String evaluate(Double lat, Double lon) throws HiveException {
    return evaluate(lat, lon, 4);
  }
}
