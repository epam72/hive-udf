package epam.udf;

import junit.framework.TestCase;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.junit.Assert;
import org.junit.Test;

public class GeoHashTest extends TestCase {
  double latitude = 10.10;
  double longitude = 10.10;
  int accuracy = 6;
  private static final GeoHash gh = new GeoHash();

  @Test
  public void testNulls() {

    String outHash = "";

    // null accuracy is the same as 4
    try {
      outHash = gh.evaluate(latitude, longitude);
    } catch (Throwable th) {

    } finally {
      Assert.assertEquals(getOriginalHash(latitude, longitude, 4), outHash);
    }

    Assert.assertThrows(HiveException.class, () -> gh.evaluate(latitude, longitude, null));
    Assert.assertThrows(HiveException.class, () -> gh.evaluate(latitude, longitude, 40));

    try {
      outHash = gh.evaluate(latitude, null, 4);
    } catch (Throwable th) {

    } finally {
      assertNull(outHash);
    }

    try {
      outHash = gh.evaluate(null, null, 4);
    } catch (Throwable th) {

    } finally {
      assertNull(outHash);
    }

    try {
      outHash = gh.evaluate(null, longitude, 4);
    } catch (Throwable th) {

    } finally {
      assertNull(outHash);
    }
  }

  @Test
  public void testEvaluate() {
    String hash1 = null;
    String hash2 = null;

    try {
      hash1 = gh.evaluate(latitude, longitude);
      hash2 = gh.evaluate(latitude, longitude, 4);
    } catch (Throwable th) {

    } finally {
      Assert.assertNotNull(hash1);
      Assert.assertNotNull(hash2);
      Assert.assertEquals(hash1, hash2);
    }
  }

  private String getOriginalHash(double lat, double lon, int acc) {
    return ch.hsr.geohash.GeoHash.withCharacterPrecision(lat, lon, acc).toBase32();
  }
}